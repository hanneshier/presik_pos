import os
import base64
import requests
import tempfile
from datetime import date
import simplejson as json

try:
    from app.models import MODELS
except:
    from models import MODELS

try:
    from app.commons.common import slugify, file_open
except:
    from commons.common import slugify, file_open


def encoder(obj):
    # FIXME: add datetime, buffer, bytes
    if isinstance(obj, date):
        return {
            '__class__': 'date',
            'year': obj.year,
            'month': obj.month,
            'day': obj.day,
        }
    raise TypeError(repr(obj) + " is not JSON serializable")


class FastModel(object):

    def __init__(self, model, ctx, fields=None):
        self.model = model
        self.ctx = ctx
        api_url = ctx['params']['api_url']
        db = ctx['params']['database']
        self.api = '/'.join(['http:/', api_url, db])
        _model = MODELS.get(model)
        self.fields = None
        if fields:
            self.fields = fields
        elif _model.get('fields'):
            self.fields = _model['fields']

    def __getattr__(self, name, *args):
        'Return attribute value'
        self.method = name
        return self

    def find(self, domain, order=None, limit=1000, ctx=None, fields=None):
        if ctx:
            self.ctx.update(ctx)
        route = self.get_route('search')
        if not fields:
            fields = self.fields
        args_ = {
            'model': self.model,
            'domain': domain,
            'order': order,
            'limit': limit,
            'fields': fields,
            'context': self.ctx,
        }
        data = json.dumps(args_, default=encoder)
        res = requests.post(route, data=data)
        return res.json()

    def write_many(self, ids, values, fields=None):
        route = self.get_route('save_many')
        if not fields:
            fields = self.fields
        args_ = {
            'model': self.model,
            'ids': ids,
            'values': values,
            'fields': fields,
            'context': self.ctx,
        }
        data = json.dumps(args_, default=encoder)
        res = requests.put(route, data=data)
        return res.json()

    def write(self, ids, values, fields=None):
        route = self.get_route('save')

        if not fields:
            fields = self.fields
        if values.get('rec_name'):
            _ = values.pop('rec_name')
        args_ = {
            'model': self.model,
            'id': ids[0],
            'ids': ids,
            'record_data': values,
            'fields': fields,
            'context': self.ctx,
        }
        data = json.dumps(args_, default=encoder)
        res = requests.put(route, data=data)
        return res.json()

    def create(self, values):
        route = self.get_route('create')
        if values.get('rec_name'):
            _ = values.pop('rec_name')
        args_ = {
            'model': self.model,
            'record': values,
            'context': self.ctx,
        }
        data = json.dumps(args_, default=encoder)
        res = requests.post(route, data=data)
        return res.json()

    def delete(self, ids):
        route = self.get_route('delete')
        args_ = {
            'model': self.model,
            'ids': ids,
            'context': self.ctx,
        }
        data = json.dumps(args_, default=encoder)
        res = requests.delete(route, data=data)
        return res.json()

    def get_route(self, target):
        route = self.api + '/' + target
        return route

    def __call__(self, values=None):
        args_ = {
            'model': self.model,
            'method': self.method,
            'args': values,
            'context': self.ctx,
        }
        route = self.get_route('model_method')
        data = json.dumps(args_, default=encoder)
        res = requests.post(route, data=data)
        response = None
        try:
            response = res.json()
        except ValueError:
            pass
        return response


class FastReport(object):

    def __init__(self, ctx):
        self.ctx = ctx
        api_url = ctx['params']['api_url']
        db = ctx['params']['database']
        self.api = '/'.join(['http:/', api_url, db])

    def get_route(self, target):
        route = self.api + '/' + target
        return route

    def get(self, values):
        route = self.get_route('report')
        args_ = {
            'report': values['report_name'],
            'args': values['args'],
            'context': self.ctx['params'],
        }
        data = json.dumps(args_, default=encoder)
        res = requests.post(route, data=data)
        return res.json()

    def open(self, args):
        oext = args['oext']
        content = args['content']
        direct_print = args['direct_print']
        name = args['name']

        dtemp = tempfile.mkdtemp(prefix='tryton_')
        fp_name = os.path.join(
            dtemp, slugify(name) + os.extsep + slugify(oext)
        )
        print('archivo :', dtemp, fp_name)

        content_data = base64.b64decode(content)
        with open(fp_name, 'wb') as file_d:
            file_d.write(content_data)
        file_open(fp_name, type, direct_print=direct_print)


if __name__ == "__main__":
    test_model = False
    test_report = True
    ctx = {
        'company': 1,
        'user': 1,
        'params': {'api_url': 'localhost:5070', 'database': 'DEMO50'}
    }
    if test_model:
        model = {'model': 'sale.sale'}
        test_model = FastModel(model, ctx)
        id = 180
        data = {
            'reference': 'OC-02874',
        }
        res = test_model.write([id], data)

    if test_report:
        data = {
            'report_name': 'sale_pos_frontend.sale_square_box_report',
            'args': {
                'date': '2021-01-12',
                'turn': 1,
                'company': 1,
                'shop': 1,
            },
        }
        report = FastReport(ctx)
        res = report.get(data)
        report.open(res)
