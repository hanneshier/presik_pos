from PyQt5.QtCore import QThread, pyqtSignal


class DoInvoice(QThread):
    """
    Process invoices using a thread
    """
    sigDoInvoice = pyqtSignal()

    def __init__(self, main, context):
        QThread.__init__(self)

    def run(self):
        self.sigDoInvoice.emit()
