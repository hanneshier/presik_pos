
MODELS = {
    'ir.module': {
        'rec_name': 'name',
        'fields': [
            'name', 'state'
        ]
    },
    'company.company': {
        'rec_name': 'rec_name',
        'fields': [
            'party', 'logo'
        ],
        'binaries': ['logo']
    },
    'res.user': {
        'rec_name': 'name',
        'fields': ['name', 'sale_device']
    },
    'product.category': {
        'rec_name': 'name',
        'fields': [
            'name', 'leasable', 'parent', 'childs', 'name_icon', 'accounting'
        ]
    },
    'sale.sale': {
        'rec_name': 'number',
        'fields': [
            'number', 'party', 'lines', 'sale_date', 'state', 'position',
            'total_amount_cache', 'salesman', 'payment_term', 'payments',
            'total_amount', 'residual_amount', 'paid_amount', 'untaxed_amount',
            'tax_amount', 'delivery_charge', 'price_list', 'invoice_number',
            'shipment_address', 'channel', 'delivery_way', 'consumer', 'kind',
            'state_string', 'shop', 'delivery_party', 'reference', 'comment',
            'delivery_state', 'table_assigned', 'invoice_type', 'net_amount',
            'tip_amount', 'delivery_amount', 'commission', 'agent', 'source',
            'payment_method', 'order_status', 'consumer.name', 'consumer.phone',
            'consumer.address', 'consumer.notes', 'consumer.delivery',
        ]
    },
    'sale.line': {
        'rec_name': 'product',
        'fields': [
            'product', 'product.template.sale_price_w_tax', 'type',
            'quantity', 'unit_price_w_tax', 'product.description', 'note',
            'description', 'qty_fraction', 'amount_w_tax', 'unit.symbol',
            'product.template.name', 'product.code', 'unit.digits', 'amount',
            'discount_rate', 'product.sale_price_w_tax', 'product.quantity',
            'order_sended', 'product.sale_price_taxed'
        ]
    },
    'account.statement.journal': {
        'rec_name': 'number',
        'fields': ['name', 'require_voucher', 'kind']
    },
    'company.employee': {
        'rec_name': 'rec_name',
        'fields': ['party', 'code']
    },
    'commission.agent': {
        'rec_name': 'rec_name',
        'fields': [
            'active', 'party', 'party.id_number', 'rec_name', 'plan.percentage'
        ]
    },
    'sale.delivery_party': {
        'rec_name': 'rec_name',
        'fields': [
            'active', 'party', 'party.id_number', 'party.phone', 'rec_name',
            'number_plate', 'type_vehicle',
        ]
    },
    'sale.discount': {
        'rec_name': 'rec_name',
        'fields': ['active', 'name', 'rec_name', 'discount']
    },
    'sale.source': {
        'rec_name': 'name',
        'fields': [
            'id', 'name', 'party', 'party.invoice_address',
                'party.shipment_address'
            ]
    },
    'party.consumer': {
        'rec_name': 'rec_name',
        'fields': ['id_number', 'rec_name', 'name', 'address',
                   'phone', 'notes', 'delivery']
    },
    'account.invoice.payment_term': {
        'rec_name': 'name',
        'fields': ['name', 'active', 'payment_type']
    },
    'sale.device': {
        'rec_name': 'name',
        'fields': [
            'name', 'shop', 'shop.company', 'shop.name', 'shop.taxes',
            'shop.party', 'journals', 'shop.product_categories', 'journal',
            'shop.payment_term', 'shop.warehouse', 'shop.discount_pos_method',
            'shop.salesman_pos_required', 'shop.electronic_authorization',
            'shop.invoice_copies', 'shop.pos_authorization', 'shop.discounts',
            'shop.computer_authorization', 'shop.manual_authorization',
            'shop.credit_note_electronic_authorization', 'shop.salesmans',
            'shop.debit_note_electronic_authorization', 'shop.delivery_man',
        ]
    },
    'sale.shop': {
        'rec_name': 'name',
        'fields': ['taxes', 'product_categories', 'party', 'invoice_copies',
                   'warehouse', 'payment_term', 'salesmans', 'delivery_man',
                   'discounts']
    },
    'product.product': {
        'rec_name': 'rec_name',
        'fields': [
            'name', 'code', 'barcode', 'write_date', 'description',
            'template.sale_price_w_tax', 'template.account_category',
            'location.name', 'image', 'image_icon', 'quantity', 'list_price',
            'encoded_sale_price', 'location', 'uom', 'categories',
            'products_mix', 'products_mix.code', 'products_mix.name'
        ],
        'binaries': ['image']
    },
    'party.party': {
        'rec_name': 'name',
        'fields': [
            'name', 'id_number', 'addresses', 'phone', 'customer_payment_term',
            'customer_payment_term.name', 'credit_limit_amount',  'receivable',
            'salesman', 'credit_amount', 'street', 'categories_string',
            'invoice_type'
        ]
    },
    'ir.action.report': {
        'rec_name': 'report_name',
        'fields': ['action', 'report_name']
    },
    'sale.configuration': {
        'rec_name': 'id',
        'fields': [
            'tip_product', 'tip_product.code', 'show_description_pos',
            'show_position_pos', 'show_stock_pos', 'password_force_assign',
            'tip_rate', 'show_agent_pos', 'discount_pos_method', 'show_brand',
            'show_location_pos', 'show_delivery_charge', 'use_price_list',
            'decimals_digits_quantity', 'password_admin_pos', 'show_fractions',
            'new_sale_automatic', 'default_invoice_type', 'show_product_image',
            'print_invoice_payment', 'delivery_product', 'encoded_sale_price',
            'delivery_product.list_price', 'delivery_product.code', 'allow_discount_handle',
            'cache_products_local',  'show_party_categories', 'delivery_party', 'print_lines_product',
        ]
    },
    'party.address': {
        'rec_name': 'name',
        'fields': [
            'name', 'street'
        ]
    },
    'sale.shop.table': {
        'rec_name': 'name',
        'fields': ['id', 'name', 'state', 'sale']
    },
    'account.tax': {
        'rec_name': 'name',
        'fields': [
            'name'
        ]
    },
    'account.statement': {
        'rec_name': 'name',
        'fields': [
            'name', 'expenses_daily', 'journal', 'turn',
        ]
    },
    'sale_pos.expenses_daily': {
        'rec_name': 'invoice_number',
        'fields': [
            'amount', 'statement', 'invoice_number', 'reference', 'party',
        ]
    },
    'commission': {
        'rec_name': 'rec_name',
        'fields': [
            'name'
        ]
    },
}
