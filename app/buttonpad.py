import os
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (
    QWidget, QGridLayout, QHBoxLayout, QStackedWidget, QVBoxLayout, QFrame
)

from app.commons.image import Image
from .commons.custom_button import CustomButton
from .tools import get_icon
from .constants import FILE_BANNER

__all__ = ['ButtonsFunction', 'ButtonsStacked', 'StartButtons']

DIR_SHARE = os.path.abspath(os.path.normpath(
    os.path.join(__file__, '..', '..', 'share'))
)


class StartButtons(QVBoxLayout):

    def __init__(self, parent):
        super(StartButtons, self).__init__()
        grid = QGridLayout()
        frame = QFrame()
        pixmap_pos = Image(self, 'pixmap_pos', FILE_BANNER)
        self.addWidget(pixmap_pos, 0)
        self.addLayout(grid, 0)
        self.addWidget(frame, 1)
        grid.setHorizontalSpacing(2)
        grid.setVerticalSpacing(2)
        grid.rowStretch(1)
        grid.parent = parent
        columns = 4
        rows = 3

        values = [
            ['button_start_take_away', 'PARA LLEVAR', 'action_start_take_away', 'take_away'],
            ['button_start_delivery', 'DOMICILIO', 'action_start_delivery', 'delivery'],
        ]

        if parent.enviroment == 'restaurant':
            values.extend([
                ['button_start_table', 'A LA MESA', 'action_start_table', 'table'],
                ['button_start_catering', 'CATERING', 'action_start_catering', 'catering'],
            ])
        values.extend([
            ['button_start_search_order', 'BUSCAR ORDEN', 'action_search_sale', 'search_sale'],
            ['button_print_sale', 'IMPRIMIR', 'action_print_sale', 'print_sale'],
        ])

        if parent.enviroment == 'restaurant':
            values.append(
                ['button_tables', 'VER MESAS', 'action_tables', 'tables']
            )

        if parent.type_pos_user in ('cashier', 'frontend', 'frontend_admin'):
            values.extend([
                ['button_control_panel', 'PANEL DE CONTROL', 'action_control_panel', 'settings'],
                ['button_reports', 'REPORTES', 'action_reports', 'reports'],
                ['button_historic_sales', 'HISTORIAL', 'action_historic_sales', 'sales_history'],
            ])

        values.append(
            ['button_help', 'AYUDA', 'action_help', 'help'],
        )

        positions = [(i, j) for i in range(rows) for j in range(columns)]
        for position, value in zip(positions, values):
            button = CustomButton(
                parent,
                id=value[0],
                size=parent.screen_size,
                icon=get_icon(value[3]),
                title=value[1],
                method=value[2],
                name_style='toolbar'
            )
            grid.addWidget(button, *position)


class ButtonsFunction(QGridLayout):

    def __init__(self, parent, values=[]):
        super(ButtonsFunction, self).__init__()

        rows = 4
        columns = 3
        self.setHorizontalSpacing(2)
        self.setVerticalSpacing(2)
        self.parent = parent
        self.set_values(values)
        if self.parent.enviroment == 'restaurant':
            rows = 3
            columns = 5
        name_style = 'toolbar'

        positions = [(i, j) for i in range(rows) for j in range(columns)]
        for position, value in zip(positions, self.values):
            name_icon = value[0][7:]
            button = CustomButton(
                parent,
                id=value[0],
                size=parent.screen_size,
                icon=get_icon(name_icon),
                title=value[1],
                method=value[2],
                name_style=name_style
            )
            self.addWidget(button, *position)
            last_row = position[0]
        self.setRowStretch(last_row + 1, 1)

    def set_values(self, values):
        if values:
            self.values = values
            return
        self.values = [
            ['button_search_product', 'PRODUCTOS', 'action_search_product'],
        ]

        self.values.extend([
            ['button_party', 'CLIENTE', 'action_party'],
            ['button_cancel', 'CANCELAR', 'action_cancel'],
            ['button_search_sale', 'BUSCAR ORDEN', 'action_search_sale'],
            ['button_print_sale', 'IMPRIMIR', 'action_print_sale'],
            ['button_delivery_party', 'DOMICILIARIO', 'action_delivery_party'],
            ['button_position', 'POSICION', 'action_position'],
            ['button_print_order', 'ENVIAR ORDEN', 'action_send_order'],
            ['button_channel', 'CANALES', 'action_source'],
        ])

        if self.parent.enviroment == 'restaurant':
            rest_options = [
                ['button_delivery', 'AGREGAR DOMICILIO', 'action_delivery'],
                ['button_comment', 'NOTA', 'action_comment'],
                ['button_tip', 'AGREGAR PROPINA', 'action_tip'],
                ['button_tables', 'MESAS', 'action_tables'],
                ['button_consumer', 'CONSUMIDOR', 'action_consumer'],
                ['button_split_sale', 'DIVIDIR CUENTA', 'action_split_sale'],
            ]
            self.values.extend(rest_options)
        else:
            self.values.extend([['button_product_info', 'INFO. PRODUCTO', 'action_info_product']])


class ButtonsStacked(QWidget):

    def __init__(self, parent):
        super(ButtonsStacked, self).__init__()
        self.stacked = QStackedWidget()
        _size = 100
        hbox = QHBoxLayout()
        if parent.screen_size == 'small':
            _size = 70
        self.stacked.setMaximumHeight(_size)

        self.button_accept = CustomButton(
            id='button_accept',
            parent=parent,
            icon=get_icon('accept'),
            title='FINALIZAR',
            name_style='toolbar',
            method='button_accept_pressed'
        )
        self.button_checkout = CustomButton(
            id='button_checkout',
            parent=parent,
            icon=get_icon('cash'),
            title='FACTURAR',
            name_style='toolbar',
            method='button_checkout_pressed'
        )
        self.button_payment_method = CustomButton(
            id='button_payment',
            parent=parent,
            title='MEDIO DE PAGO',
            icon=get_icon('money'),
            name_style='toolbar',
            method='button_payment_pressed'
        )

        self.button_payment_term = CustomButton(
            id='button_payment_term',
            parent=parent,
            title='PLAZO DE PAGO',
            icon=get_icon('terminal_journal'),
            name_style='toolbar',
            method='action_payment_term'
        )

        if parent.type_pos_user != 'order':
            self.stacked.addWidget(self.button_accept)
            self.stacked.addWidget(self.button_checkout)
            self.stacked.addWidget(self.button_payment_method)

            hbox.addWidget(self.stacked, 0)
        if parent.type_pos_user in ('cashier', 'frontend_admin'):
            hbox.addWidget(self.button_payment_term, 0)

        if parent.type_pos_user == 'cashier' and parent.enviroment == 'retail':
            self.button_to_draft = CustomButton(
                id='button_to_draft',
                parent=parent,
                title='VOLVER A BORRADOR',
                icon=get_icon('draft'),
                name_style='toolbar',
                method='button_to_draft_pressed'
            )
            hbox.addWidget(self.button_to_draft, 0)

        if parent.type_pos_user == 'order' or parent.type_pos_user == 'salesman' and parent.enviroment == 'retail':
            self.button_send_order = CustomButton(
                id='button_send_to_pay',
                icon=get_icon('draft'),
                parent=parent,
                title='IR A PAGAR',
                method='button_send_to_pay_pressed',
                name_style='toolbar'
            )
            hbox.addWidget(self.button_send_order, 0)
        self.setLayout(hbox)


class ButtonsNumber(QGridLayout):

    def __init__(self, parent):
        # Numpad for Numbers
        super(ButtonsNumber, self).__init__()
        self.setHorizontalSpacing(1)
        self.setVerticalSpacing(1)


class Buttonpad(QWidget):

    def __init__(self, parent):
        super(Buttonpad, self).__init__()
        self._text = ''
        self._keyStates = {}
        self.set_keys()

    def set_keys(self):
        q = Qt
        self.keys_numbers = list(range(q.Key_0, q.Key_9 + 1))
        self.keys_alpha = list(range(q.Key_A, q.Key_Z + 1))
        self.keys_special = [
            q.Key_Asterisk, q.Key_Comma, q.Key_Period,
            q.Key_Minus, q.Key_Slash]
        self.show_keys = self.keys_numbers + self.keys_alpha + self.keys_special
