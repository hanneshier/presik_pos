import os
from PyQt5.QtCore import QSettings


class Params(object):
    """
    Params Configuration
    This class load all settings from .ini file
    """

    def __init__(self, file_):
        self.file = file_

        dirx = os.path.abspath(os.path.join(__file__, '..', '..'))

        if os.name == 'posix':
            homex = 'HOME'
            dirconfig = '.tryton'
        elif os.name == 'nt':
            homex = 'USERPROFILE'
            dirconfig = 'AppData/Local/tryton'

        HOME_DIR = os.getenv(homex)
        default_dir = os.path.join(HOME_DIR, dirconfig)

        if os.path.exists(default_dir):
            config_file = os.path.join(default_dir, self.file)
        else:
            config_file = os.path.join(dirx, self.file)

        if not os.path.exists(config_file):
            config_file = self.file

        settings = QSettings(config_file, QSettings.IniFormat)

        self.params = {}
        for key in settings.allKeys():
            if key[0] == '#':
                continue
            self.params[key] = settings.value(key, None)
