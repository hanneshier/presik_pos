# This file is part of Neo.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

import logging
import socket
from .jsonrpc import ServerProxy, Fault

CONNECTION = None
_USER = None
_USERNAME = ''
_HOST = ''
_PORT = None
_DATABASE = ''
CONTEXT = {}


def server_version(host, port):
    try:
        connection = ServerProxy(host, port)
        logging.getLogger(__name__).info(
            'common.server.version(None, None)')
        result = connection.common.server.version()
        logging.getLogger(__name__).debug(repr(result))
        return result
    except (Fault, socket.error):
        raise


def _execute(conn, *args):
    global CONNECTION, _USER
    name = '.'.join(args[:3])
    args = args[3:]
    result = getattr(conn.server, name)(*args)
    return result


def execute(conn, *args):
    return _execute(conn, *args)


class RPCProgress(object):

    def __init__(self, conn, method, args):
        self.method = method
        self.args = args
        self.conn = conn
        self.res = None
        self.error = False
        self.exception = None

    def run(self):
        try:
            res = execute(self.conn, *self.args)
        except:
            pass
        return res
