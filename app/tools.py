import os
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDesktopWidget


current_dir = os.path.dirname(__file__)


def get_icon(name):
    name = name if name else 'fork'
    path_icon = os.path.join(current_dir, 'share', name + '.svg')
    return QIcon(path_icon)


def to_numeric(number):
    return str(round(number, 2))


def to_float(number, digits):
    return str(round(number, 4))


def get_screen():
    screen = QDesktopWidget().screenGeometry()
    width = screen.width()
    height = screen.height()
    return width, height
