#!/usr/bin/env python3

from app.reporting import Receipt


if __name__ == '__main__':

    # Test for Escpos interface printer Linux and Windows

    # Network example
    # device = 'network', '192.168.0.32'

    # Unix-like Usb example
    # device = 'usb', '/dev/usb/lp1'

    # Windows Usb example for printer name SATPOS
    # device = 'usb', 'SATPOS'

    # SSH example
    # device = 'ssh', 'psk@xxxxx@192.168.0.5@23@/dev/usb/lp1'

    protocols = {
        '1': 'usb',
        '2': 'network',
        '3': 'ssh',
    }
    protocols_msg = """
        1 - usb
        2 - network
        3 - ssh
    """
    print('Please select interface...')
    print(protocols_msg)
    interface = input('Interface : ')

    print("""
        Please insert device path, examples...

        # Network example:
            192.168.0.32

        # Unix-like USB example
            /dev/usb/lp1

        # Windows USB example for printer name SATPOS
            EPSON-POS
    """)

    device = input('Device : ')
    protocol = protocols[str(interface)]
    printer_test = {
        'interface': protocol,
        'device': str(device),
        'profile': 'TM-P80',
    }

    ctx_printing = {}
    ctx_printing['company'] = 'OSCORP INC'
    ctx_printing['sale_device'] = 'CAJA-10'
    ctx_printing['shop'] = 'Shop Wall Boulevard'
    ctx_printing['street'] = 'Cll 21 # 172-81. Central Park'
    ctx_printing['user'] = 'Charles Chapplin'
    ctx_printing['city'] = 'Dallas'
    ctx_printing['zip'] = '0876'
    ctx_printing['phone'] = '591 5513 455'
    ctx_printing['id_number'] = '123456789-0'
    ctx_printing['tax_regime'] = 'none'

    receipt = Receipt(ctx_printing)
    if 1: #try:
        receipt.config_printer(printer_test)
        receipt.test_printer()
    # except:
    #     print('Printing failed...!')
